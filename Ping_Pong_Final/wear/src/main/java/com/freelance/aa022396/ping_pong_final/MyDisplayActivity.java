package com.freelance.aa022396.ping_pong_final;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class MyDisplayActivity extends Activity {

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        onNewIntent(getIntent());

    }


    @Override
    public void onNewIntent(Intent intent){
        mTextView = (TextView) findViewById(R.id.text);
        mTextView.setText(intent.getStringExtra("notificationText"));
    }

}
