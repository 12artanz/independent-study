package com.freelance.aa022396.ping_pong_final;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by aa022396 on 4/13/16.
 */
public class WearMessageListener extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks{

    private static final String TAG = "ExampleMessageApp";

    private static final int MESSAGE_RECEIVED_NOTIFICATION_ID = 1;

    private static final String RECEIVE_DECRYPTED_CAPABILITY_NAME= "MessageTest_receive_decrypted_message";
    private static final String RECEIVE_ENCRYPTED_MESSAGE_CAPABILITY_NAME = "receive_encrypted_message";
    private static final String RECEIVE_ENCRYPTED_MESSAGE_MESSAGE_PATH = "/receive_encrypted_message";

    private static final String DECRYPT_FILE = "/decrypt_file";
    private static final String ENCRYPT_FILE = "/encrypt_file";

    private static final String KEY_FILE_NAME = "keys";


    private GoogleApiClient mGoogleApiClient;



    @Override
    public void onCreate(){
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .build();
    }
    @Override
    public void onDestroy(){
        if (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    @Override
    public void onConnectedNodes(List<Node> connectedNodes) {
        if (mGoogleApiClient.isConnected()) {
            setOrUpdateNotification();
        } else if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    private void setOrUpdateNotification(){
        Wearable.CapabilityApi.getCapability(mGoogleApiClient, RECEIVE_DECRYPTED_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE).setResultCallback(new ResultCallback<CapabilityApi.GetCapabilityResult>() {
            @Override
            public void onResult(CapabilityApi.GetCapabilityResult result) {
                if (result.getStatus().isSuccess()){
                    updateMessageCapability(result.getCapability());
                }else{
                    Log.e(TAG,
                            "setOrUpdateNotification() Failed to get capabilities, "
                                    + "status: "
                                    + result.getStatus().getStatusMessage());
                }
            }
        });
    }

    private void updateMessageCapability(CapabilityInfo capabilityInfo){
        Set<Node> connectedNodes = capabilityInfo.getNodes();
        if (connectedNodes.isEmpty()) {
            setupLostConnectivityNotification();
        } else {
            for (Node node : connectedNodes) {
                if (node.isNearby()) {
                    ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                            .cancel(MESSAGE_RECEIVED_NOTIFICATION_ID);
                }
            }
        }
    }

    private void setupLostConnectivityNotification() {
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Title")
                .setContentText("Text")
                .setVibrate(new long[]{0, 200})  // Vibrate for 200 milliseconds.
                .setLocalOnly(true)
                .setPriority(Notification.PRIORITY_MAX);
        Notification card = notificationBuilder.build();
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                .notify(MESSAGE_RECEIVED_NOTIFICATION_ID, card);
    }


    /**********************************************************************************************
     *
     * GoogleApiClient.ConnectionCallbacks and GoogleApiClient.OnConnectionFailedListener methods
     *
     *********************************************************************************************/

    @Override
    public void onConnected(Bundle bundle) {
        setOrUpdateNotification();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Handles the messages sent by the mobile device.
     * Splits them into two categories:
     * Those requesting permission to encrypt
     * Those requesting permission to decrypt
     * @param messageEvent
     */
    @Override
    public void onMessageReceived(MessageEvent messageEvent){
        System.out.println("Here's the path I received: " + new String(messageEvent.getPath()));
        if(new String(messageEvent.getPath()).equals(DECRYPT_FILE)){
            System.out.println("Decrypt Message received.");
            System.out.println("Message text: " + messageEvent.getData());
            System.out.println("Message text round 2?: " + new String(messageEvent.getData()));

            String messageReceived = decryptMessage(messageEvent.getData());
            String [] messageParts = messageReceived.split(",");
            String filePath = messageParts[0];
            System.out.println("Here's the file path I'm looking for: " + filePath);
            double currentLat = Double.parseDouble(messageParts[1]);
            double currentLong = Double.parseDouble(messageParts[2]);

            System.out.println("This is the decrypted message: " + messageReceived);


            String messageToSend = null;
            try{
                File file = new File(getFilesDir(), KEY_FILE_NAME);
                FileInputStream fis = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader bufferedReader = new BufferedReader(isr);
                String line;
                while((line = bufferedReader.readLine()) != null){
                    String [] lineParts = line.split(",");
                    if(!lineParts[0].equals(filePath)){
                        continue;
                    }
                    float[] results = new float[3];
                    double storedLat = Double.parseDouble(lineParts[1]);
                    double storedLong = Double.parseDouble(lineParts[2]);
                    Location.distanceBetween(currentLat,currentLong,storedLat,storedLong, results);
                    if(results[0] <10){
                        messageToSend = lineParts[0] + "," +lineParts[3];
                        System.out.println("Here's what the encoded key looks like when pulled from storage: " + lineParts[3]);
                        break;
                    }
                    System.out.println("distance was bad");//messageToSend is null
                }
            }catch(Exception e){
                e.printStackTrace();
            }

            postNotification("Received request from phone to decrypt file.", DECRYPT_FILE, messageToSend);
        }
        else if(new String(messageEvent.getPath()).equals(ENCRYPT_FILE)){
            System.out.println("Encrypt Message received.");
            System.out.println("Message text: " + messageEvent.getData());
            System.out.println("Message text round 2?: " + new String(messageEvent.getData()));

            String messageToPass = decryptMessage(messageEvent.getData());

            System.out.println("This is the decrypted message: " + messageToPass);

            postNotification("Received request from phone to encrypt file.", ENCRYPT_FILE, messageToPass);
        }

    }

    /**
     * Takes the information processed from the message received and creates a wearable notification
     * No mobile actions taken until user okays through the notification
     * @param notificationText
     * @param path
     * @param messageToSend
     */
    private void postNotification(String notificationText, String path, String messageToSend){
        Intent notificationIntent = new Intent(this, MyDisplayActivity.class);
        notificationIntent.putExtra("notificationText", notificationText);


        Intent actionIntent = new Intent(this, WearMessageSender.class);
        actionIntent.putExtra("path", path);
        actionIntent.putExtra("message", messageToSend);
        actionIntent.setAction("com.freelance.aa022396.ping_pong_final.WearMessageSender");

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingReplyIntent = PendingIntent.getActivity(this, 0, actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .extend(new Notification.WearableExtender()
                                .setDisplayIntent(pendingNotificationIntent)
                )
                .setContentIntent(pendingReplyIntent)
                .setAutoCancel(true)
                .setVibrate(new long[]{0, 200})  // Vibrate for 200 milliseconds.
                .build();
        ((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).notify(0, notification);
    }


    /**
     * Decrypts the message sent from the mobile device with the secret symmetric key
     * @param messageToDecrypt
     * @return
     */
    private String decryptMessage(byte [] messageToDecrypt){
        String decryptedMessage = "";
        byte[] keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptedMessage = new String(cipher.doFinal(messageToDecrypt), "UTF-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        return decryptedMessage;
    }



}
