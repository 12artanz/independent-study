package com.freelance.aa022396.ping_pong_final;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class WearMessageSender extends WearableActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    private static final SimpleDateFormat AMBIENT_DATE_FORMAT =
            new SimpleDateFormat("HH:mm", Locale.US);

    private BoxInsetLayout mContainerView;
    private TextView mTextView;
    private TextView mClockView;
    private GoogleApiClient mGoogleApiClient;

    private static final String DECRYPT_FILE = "/decrypt_file";
    private static final String ENCRYPT_FILE = "/encrypt_file";
    private static final String KEY_FILE_NAME = "keys";

    /**********************************************************************************************
     *
     * Activity Lifecycle State methods
     *
     *********************************************************************************************/
    @Override
    public void onStart(){
        super.onStart();
        System.out.println("Gonna connect now");
        mGoogleApiClient.connect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wear_message_sender);
        setAmbientEnabled();

        mContainerView = (BoxInsetLayout) findViewById(R.id.container);
        mTextView = (TextView) findViewById(R.id.text);
        mClockView = (TextView) findViewById(R.id.clock);

        System.out.println("Setting up the GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
    }

    /*******************************************************************************************
     *
     * Wearable Activity display methods
     *
     * ****************************************************************************************/

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        updateDisplay();
    }

    @Override
    public void onUpdateAmbient() {
        super.onUpdateAmbient();
        updateDisplay();
    }

    @Override
    public void onExitAmbient() {
        updateDisplay();
        super.onExitAmbient();
    }

    private void updateDisplay() {
        if (isAmbient()) {
            mContainerView.setBackgroundColor(getResources().getColor(android.R.color.black));
            mTextView.setTextColor(getResources().getColor(android.R.color.white));
            mClockView.setVisibility(View.VISIBLE);

            mClockView.setText(AMBIENT_DATE_FORMAT.format(new Date()));
        } else {
            mContainerView.setBackground(null);
            mTextView.setTextColor(getResources().getColor(android.R.color.black));
            mClockView.setVisibility(View.GONE);
        }
    }


    /**
     * Sends message to mobile device granting permission to decrypt/encrypt.
     * Stops if the message it pulls from the intent is null.
     * Null message means that it was unable to find the file requested or user is not at a way point
     */
    public void sendMessage(){
        Intent intent = getIntent();
        String path = intent.getStringExtra("path");
        String message = intent.getStringExtra("message");
        System.out.println("Here's the message that I was packed with: " + message);
        if(message == null){
            WearMessageSender.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(WearMessageSender.this, "File isn't encrypted or not in the right place to open it.", Toast.LENGTH_LONG).show();
                }
            });
//            Toast.makeText(WearMessageSender.this, "File doesn't exist or not in the right place to open it.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(path.equals(ENCRYPT_FILE)){
            try{
                if(!getFilesDir().exists()){
                    getFilesDir().mkdir();//Creates the files directory if it doesn't exist
                }

                File file = new File(getFilesDir(), KEY_FILE_NAME);
                System.out.println(file.getAbsolutePath());


                if(!file.exists()){
                    file.createNewFile();
                    FileOutputStream fos = new FileOutputStream(file);
                    OutputStreamWriter writer = new OutputStreamWriter(fos);
                    writer.write(message);
                    writer.flush();
                    writer.close();
                }
                else {
                    //Gauntlet to determine if the file to encrypt is within an existing way point
                    boolean withinWayPoint = false;
                    String [] messageParts = message.split(",");
                    FileInputStream fis = new FileInputStream(file);
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader bufferedReader = new BufferedReader(isr);
                    String line;
                    while((line = bufferedReader.readLine()) != null){
                        System.out.println("Looper says" + line);
                        String [] lineParts = line.split(",");
                        if(lineParts[0].equals(messageParts[0])){
                            float[] results = new float[3];
                            double storedLat = Double.parseDouble(lineParts[1]);
                            double storedLong = Double.parseDouble(lineParts[2]);
                            double currentLat = Double.parseDouble(messageParts[1]);
                            double currentLong = Double.parseDouble(messageParts[2]);
                            Location.distanceBetween(currentLat, currentLong, storedLat, storedLong, results);
                            if(results[0] < 10){
                                withinWayPoint = true;
                                message = lineParts[0] + "," + lineParts[1] + "," + lineParts[2] + "," + lineParts[3];
                            }
                        }
                    }

                    if(!withinWayPoint){
                        FileOutputStream fos = new FileOutputStream(file, true);
                        OutputStreamWriter writer = new OutputStreamWriter(fos);
                        writer.append(message);
                        writer.flush();
                        writer.close();
                    }

                }
            }catch(Exception e){
                e.printStackTrace();
            }
                        //Maybe access from file instead
            String [] messageParts = message.split(",");
            message = messageParts[0] + "," +messageParts[3];
            System.out.println(message);
        }

        byte [] messageToSend = null;
        byte[] input = message.getBytes();
        byte[] keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            messageToSend = cipher.doFinal(input);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for(Node node: nodes.getNodes()){
            System.out.println("Sending Message");
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient, node.getId(),path, messageToSend).await();
        }
    }


    /**********************************************************************************************
     *
     * GoogleApiClient.ConnectionCallbacks and GoogleApiClient.OnConnectionFailedListener methods
     *
     *********************************************************************************************/

    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("Starting Asynchtask.");
        new MyTask().execute();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    /**
     * Asynchronous task to send a message to mobile device.
     */
    private class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            sendMessage();
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            System.out.println("Message sent");
        }
    }

}
