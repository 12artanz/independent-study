package com.freelance.aa022396.ping_pong_final;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;


import java.io.File;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private GoogleApiClient mGoogleApiClient;

    private Location currentLocation;
    private LocationManager locationManager;


    private static final String RECEIVE_ENCRYPTED_MESSAGE_CAPABILITY_NAME = "receive_encrypted_message";
    private static final String DECRYPT_FILE = "/decrypt_file";
    private static final String ENCRYPT_FILE = "/encrypt_file";
    private String transcriptionNodeId = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .addApi(AppIndex.API)
                .build();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }


    /**
     * Sets up to send a message to wearable device requesting permission to encrypt.
     * Sends GPS coordinates to create way point from which the file can be accessed.
     * @param filePath absolute file path of file to encrypt
     */
    private void encryptFile(String filePath){
        try{
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(256);
            SecretKey secretKey = keyGen.generateKey();

            Location tempLocation = currentLocation;
            String keyEncoded = Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);
            String message = filePath + "," + tempLocation.getLatitude() + "," + tempLocation.getLongitude() + "," + keyEncoded;
            sendMessage(message, ENCRYPT_FILE);

        }catch(Exception e){
            e.printStackTrace();
        }







    }

    /**
     * Sets up to send a message to wearable device requesting permission to decrypt.
     * Sends GPS coordinates to authorize way point.
     * @param filePath absolute file path of file to decrypt
     */
    private void decryptFile(String filePath){
        Location tempLocation = currentLocation;
        String message = filePath + "," +tempLocation.getLatitude() + "," + tempLocation.getLongitude();
        sendMessage(message, DECRYPT_FILE);

    }

    /**
     * Handles the encryption and sending of the message to the wearable device.
     * @param message message to send formatted as a comma separated values.
     * @param MESSAGE_PATH denotes whether to request encryption or decryption permission
     */
    private void sendMessage(String message, String MESSAGE_PATH){
        byte [] messageToSend = null;

        byte[] input = message.getBytes();
        byte[] keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };

        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            messageToSend = cipher.doFinal(input);//Specify UTF-8?
        } catch (Exception e) {
            e.printStackTrace();
        }

        CapabilityApi.GetCapabilityResult result =
                Wearable.CapabilityApi.getCapability(
                        mGoogleApiClient, RECEIVE_ENCRYPTED_MESSAGE_CAPABILITY_NAME,
                        CapabilityApi.FILTER_ALL).await();
        updateTranscriptionCapability(result.getCapability());


        if (transcriptionNodeId != null) {
            Wearable.MessageApi.sendMessage(mGoogleApiClient, transcriptionNodeId,
                    MESSAGE_PATH, messageToSend).await();
        } else {
            System.out.println("Unable to retrieve node with transcription capability");
        }

    }




    /**
     * Updates the transcriptionNodeId with the id of the best node
     * @param capabilityInfo
     */
    private void updateTranscriptionCapability(CapabilityInfo capabilityInfo) {
        Set<Node> connectedNodes = capabilityInfo.getNodes();
        transcriptionNodeId = pickBestNodeId(connectedNodes);
    }

    /**
     * Selects the best node based on whether the node is nearby
     * @param nodes
     * @return
     */
    private String pickBestNodeId(Set<Node> nodes) {
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (node.isNearby()) {
                return node.getId();
            }
            bestNodeId = node.getId();
        }
        return bestNodeId;
    }




    /**********************************************************************************************
     *
     * Activity Lifecycle State methods
     *
     *********************************************************************************************/


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.freelance.aa022396.ping_pong_final/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.freelance.aa022396.ping_pong_final/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        mGoogleApiClient.disconnect();
    }

    /**********************************************************************************************
     *
     * GoogleApiClient.ConnectionCallbacks and GoogleApiClient.OnConnectionFailedListener methods
     *
     *********************************************************************************************/

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * Called when GoogleApiClient is ready to use.
     * OnclickListeners are set for both buttons.
     * Listeners first check to see if the filePath provided is legitimate. Then start the message.
     * @param bundle
     */
    @Override
    public void onConnected(Bundle bundle) {
        final Button encryptBTN = (Button) findViewById(R.id.encryptBTN);
        encryptBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText filePathET = (EditText)findViewById(R.id.encryptFilePathET);
                String filePath = filePathET.getText().toString();
                File file = new File(filePath);
                if(file.exists()){
                    new MyTask().execute(filePath, ENCRYPT_FILE);
                }else{
                    Toast.makeText(MainActivity.this, "That file doesn't exist.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        final Button decryptBTN = (Button)findViewById(R.id.decryptBTN);
        decryptBTN.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                EditText filePathET = (EditText)findViewById(R.id.decryptFilePathET);
                String filePath = filePathET.getText().toString();
                File file = new File(filePath);
                if(file.exists()){
                    new MyTask().execute(filePath, DECRYPT_FILE);
                }else{
                    Toast.makeText(MainActivity.this, "That file doesn't exist.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("Connection failed!!!");
    }

    /**********************************************************************************************
     *
     * LocationListener methods
     *
     *********************************************************************************************/


    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        currentLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    /**
     * Asynchronous task that is called when button's are clicked.
     */
    private class MyTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            if(params[1].equals(ENCRYPT_FILE)){
                encryptFile(params[0]);
            }else{
                decryptFile(params[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            Toast.makeText(MainActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();
        }
    }






}
