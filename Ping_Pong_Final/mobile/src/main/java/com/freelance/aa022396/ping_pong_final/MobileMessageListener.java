package com.freelance.aa022396.ping_pong_final;

import android.os.Bundle;
import android.util.Base64;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by aa022396 on 4/21/16.
 */
public class MobileMessageListener extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks {

    GoogleApiClient mGoogleApiClient;
    private static final String DECRYPT_FILE = "/decrypt_file";
    private static final String ENCRYPT_FILE = "/encrypt_file";

    @Override
    public void onCreate(){
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .build();
    }


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals(DECRYPT_FILE)) {
            String message = decryptMessage(messageEvent.getData());
            String [] messageParts = message.split(",");
            String filePath = messageParts[0];
            String strKey = messageParts[1];

            byte[] arrKey = Base64.decode(strKey, Base64.DEFAULT);
            SecretKey key = new SecretKeySpec(arrKey, 0, arrKey.length, "AES");

            writeFile(filePath, key, Cipher.DECRYPT_MODE);

        }else if(messageEvent.getPath().equals(ENCRYPT_FILE)){
            String message = decryptMessage(messageEvent.getData());
            String [] messageParts = message.split(",");
            String filePath = messageParts[0];
            String strKey = messageParts[1];


            byte[] arrKey = Base64.decode(strKey, Base64.DEFAULT);
            SecretKey key = new SecretKeySpec(arrKey, 0, arrKey.length, "AES");
            writeFile(filePath, key, Cipher.ENCRYPT_MODE);
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void writeFile(String filePath, SecretKey key, int operation){
        String line;
        String outputFile = "";

        try {
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            cipher.init(operation, key);
            StringBuilder sb = new StringBuilder();
            while ((line = file.readLine()) != null) {
                sb.append(line);
            }
            byte [] fileAsBytes = sb.toString().getBytes();
            if(operation == Cipher.ENCRYPT_MODE){
                byte [] temp = cipher.doFinal(fileAsBytes);
                outputFile += Base64.encodeToString(temp, Base64.DEFAULT);
            }
            else{
                byte [] fileDecoded = Base64.decode(sb.toString(), Base64.DEFAULT);
                byte [] temp = cipher.doFinal(fileDecoded);
                outputFile += new String(temp, "UTF-8");
            }

            file.close();

            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            OutputStreamWriter osw = new OutputStreamWriter(fileOutputStream);
            osw.write(outputFile);
            osw.flush();
            osw.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private String decryptMessage(byte [] messageToDecrypt){
        String decryptedMessage = "";
        byte[] keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptedMessage = new String(cipher.doFinal(messageToDecrypt), "UTF-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        return decryptedMessage;
    }








}
