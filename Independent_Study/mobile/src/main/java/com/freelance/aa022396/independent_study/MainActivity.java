package com.freelance.aa022396.independent_study;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
        System.out.println("GoogleApiClient is built.");


    }

    public void sendEncryptedMessage(){
        System.out.println("sendEncryptedMessage called");


/********************************************************
 *
 * Encryption Starts Here
 *
 *******************************************************/
        EditText messageET = (EditText)findViewById(R.id.messageToEncrypt);
        String messagePlaintext = messageET.getText().toString();
        Toast.makeText(MainActivity.this, messagePlaintext, Toast.LENGTH_SHORT).show();

        byte[]        input = messagePlaintext.getBytes();
        byte[]        keyBytes = new byte[] {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };

        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");

        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");


            System.out.println("input : " + new String(input, "UTF-8"));

            // encryption pass
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] cipherText = new byte[cipher.getOutputSize(input.length)];
            int ctLength = cipher.update(input, 0, input.length, cipherText, 0);
            ctLength += cipher.doFinal(cipherText, ctLength);

            System.out.println("cipher: " + new String(cipherText, "UTF-8") + " bytes: " + ctLength);

            // decryption pass
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] plainText = new byte[cipher.getOutputSize(ctLength)];
            int ptLength = cipher.update(cipherText, 0, ctLength, plainText, 0);
            ptLength += cipher.doFinal(plainText, ptLength);

            System.out.println("plain : " + new String(plainText, "UTF-8") + " bytes: " + ptLength);


        }catch(Exception e){

        }

        /*********************************************
         *
         * Encryption stops here
         *
         *********************************************/




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /************************************************
     *
     * Activity state methods
     *
     ***********************************************/
    @Override
    public void onStart(){
        super.onStart();
        System.out.println("I have started and will now connect.");
        mGoogleApiClient.connect();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }





    /*************************************************
     *
     * GoogleApiClient.ConnectionCallbacks methods
     *
     ************************************************/


    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("GoogleApiClient is connected");
        final Button sendBTN = (Button) findViewById(R.id.messageBTN);
        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("I have been clicked and will begin running.");
                new MyTask().execute();
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /*************************************************
     *
     * GoogleApiClient.OnConnectionFailedListener method
     *
     ************************************************/

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    private class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            sendEncryptedMessage();
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
        }
    }


}
