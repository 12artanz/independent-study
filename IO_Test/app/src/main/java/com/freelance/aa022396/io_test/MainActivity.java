package com.freelance.aa022396.io_test;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_FILE_NAME = "keys2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        String message = "Hello, I am Galstaff sorcerer of light!!!!!";
        System.out.println("About to start");
        try{

//            File file = new File(getFilesDir(), KEY_FILE_NAME);
//            System.out.println(getFilesDir().getAbsolutePath());
//            System.out.println(file.getAbsolutePath());
            File file = new File (Environment.getExternalStorageDirectory() + "/Documents");
            boolean temp = true;
            if(!file.exists()){
                System.out.println(file.getAbsolutePath());
                temp = file.mkdir();
                System.out.println("It didn't exist" + temp);
//                file.createNewFile();
//                FileOutputStream fos = new FileOutputStream(file);
//                OutputStreamWriter writer = new OutputStreamWriter(fos);
//                writer.write("I am a brand spanking new file!");
//                writer.flush();
//                writer.close();
            }
            if(temp){
                System.out.println("Writing to that file");
                File filePlus = new File(file.getAbsolutePath(), "test17.txt");
                System.out.println(filePlus.getAbsolutePath());
                filePlus.createNewFile();
                System.out.println(file.exists());
                FileOutputStream fos = new FileOutputStream(filePlus, true);
                OutputStreamWriter writer = new OutputStreamWriter(fos);
                writer.append(message);
                writer.flush();
                writer.close();
                System.out.println("end");
            }
            else {
//                System.out.println(file.getAbsolutePath());
//                System.out.println(file.exists());
//                FileOutputStream fos = new FileOutputStream(file, true);
//                OutputStreamWriter writer = new OutputStreamWriter(fos);
//                writer.append(message);
//                writer.flush();
//                writer.close();
//                System.out.println("end");
            }
//            FileInputStream fis = new FileInputStream(file);
//            InputStreamReader isr = new InputStreamReader(fis);
//            BufferedReader bufferedReader = new BufferedReader(isr);
//            StringBuilder sb = new StringBuilder();
//            String line;
//            while((line = bufferedReader.readLine()) != null){
//                sb.append(line);
//            }
//            System.out.println(sb.toString());
        }catch(Exception e){
            e.printStackTrace();
        }



//        try{
//            String theMessage = "Lorem ipsum";
//
//            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
//            keyGen.init(256);
//            SecretKey secretKey = keyGen.generateKey();
//
//            Cipher cipher = null;
//            cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
////            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
//            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
//            byte [] messageToSend = cipher.doFinal(theMessage.getBytes());
//            System.out.println("Encryption result: " + new String(messageToSend, "UTF-8"));
//
//
//
//            String keyEncoded = Base64.encodeToString(secretKey.getEncoded(), Base64.DEFAULT);
//            System.out.println("Here's the key encoded: " + keyEncoded);
//
//            byte [] arrKey = Base64.decode(keyEncoded, Base64.DEFAULT);
//            SecretKey secretKey1 = new SecretKeySpec(arrKey, 0, arrKey.length, "AES");
//            cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
//            cipher.init(Cipher.DECRYPT_MODE, secretKey1);
//            System.out.println(new String(cipher.doFinal(messageToSend), "UTF-8"));
////            System.out.println("This is what the encoded key looks like when made: " + keyEncoded);
////            String message = filePath + "," + keyEncoded;
////            sendMessage(message, ENCRYPT_FILE);
//
//        }catch(Exception e){
//            e.printStackTrace();
//        }

//        byte [] messageToSend = null;
//
//        byte[] input = message.getBytes();
//        byte[] keyBytes = new byte[] {
//                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
//                0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
//                0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17 };
//
//        Cipher cipher = null;
//        try {
//            cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
//            SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
//            cipher.init(Cipher.ENCRYPT_MODE, key);
//            messageToSend = cipher.doFinal(input);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
