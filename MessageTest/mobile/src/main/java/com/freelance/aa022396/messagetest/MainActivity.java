package com.freelance.aa022396.messagetest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.net.URL;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    private static final String RECEIVE_ENCRYPTED_MESSAGE_CAPABILITY_NAME = "MessageTest_receive_encrypted_message";
    public static final String RECEIVE_ENCRYPTED_MESSAGE_MESSAGE_PATH = "/MessageTest_receive_encrypted_message";


    private String transcriptionNodeId = null;

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
    }

    /**
     * Make the GoogleApiClient connect when ready
     */
    @Override
    protected void onStart(){
        super.onStart();
        System.out.println("I have started and will now connect.");
        mGoogleApiClient.connect();
    }


    private void setUpMessage(){
        System.out.println("I am officially running");
        System.out.println("Shits connected for real");
        System.out.println("Here's the proof");
        System.out.println(mGoogleApiClient.isConnected());
        System.out.println(mGoogleApiClient.hasConnectedApi(Wearable.API));
        EditText messageET = (EditText)findViewById(R.id.sendMessageET);
        String message = messageET.getText().toString();

        //Send Message
        System.out.println("Getting those nodes");
        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
//        CapabilityApi.GetCapabilityResult result =
//                Wearable.CapabilityApi.getCapability(
//                        mGoogleApiClient, RECEIVE_ENCRYPTED_MESSAGE_CAPABILITY_NAME,
//                        CapabilityApi.FILTER_ALL).await();
        System.out.println("Nodes successfully retrieved.");
//        updateTranscriptionCapability(result.getCapability());


        System.out.println("Gonna send now");
//        if (transcriptionNodeId != null) {
//            Wearable.MessageApi.sendMessage(mGoogleApiClient, transcriptionNodeId,
//                    RECEIVE_ENCRYPTED_MESSAGE_MESSAGE_PATH, message.getBytes()).setResultCallback(
//                    new ResultCallback() {
//                        @Override
//                        public void onResult(Result result) {
//                            if(!result.getStatus().isSuccess()){
//                                //Failed to send message
//                            }
//                        }
//
////                                @Override
////                                public void onResult(MessageApi.SendMessageResult sendMessageResult) {
////                                    if (!sendMessageResult.getStatus().isSuccess()) {
////                                        // Failed to send message
////                                    }
////                                }
//                    }
//            );
//            System.out.println("All sent.");
//        } else {
//            System.out.println("Unable to retrieve node with transcription capability");
//        }

        System.out.println(nodes.getNodes().size());

        for(Node node: nodes.getNodes()){
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient, node.getId(),"/start_activity", message.getBytes()).await();
            System.out.println("Message Sent.");
        }
    }



    private void updateTranscriptionCapability(CapabilityInfo capabilityInfo) {
        System.out.println("Inside Stuff");
        Set<Node> connectedNodes = capabilityInfo.getNodes();

        transcriptionNodeId = pickBestNodeId(connectedNodes);
    }

    private String pickBestNodeId(Set<Node> nodes) {
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (node.isNearby()) {
                return node.getId();
            }
            bestNodeId = node.getId();
        }
        return bestNodeId;
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("Connection failed!!!");
    }


    /***********************************
     *
     *
     * Connection Callback
     *
     **********************************/


    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("I have connected! Watch me set this button up.");
        final Button sendBTN = (Button) findViewById(R.id.sendMessageBTN);
        sendBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("I have been clicked and will begin running.");
                new MyTask().execute();
            }
        });
        System.out.println("Button's ready for pushing captain!");
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("Shits suspended.");
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }







    private class MyTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            setUpMessage();
            return null;
        }

        @Override
        protected void onPostExecute(Void result){
            Toast.makeText(MainActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();
        }
    }





}
