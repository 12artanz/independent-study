package com.freelance.aa022396.messagetest;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.CapabilityInfo;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;
import java.util.Set;

/**
 * Created by aa022396 on 3/14/16.
 */
public class MessageListener extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks{

    private static final String TAG = "ExampleMessageApp";

    private static final int MESSAGE_RECIEVED_NOTIFICATION_ID = 1;

    /* the capability that the phone app would provide */
    private static final String RECEIVE_DECRYPTED_CAPABILITY_NAME= "MessageTest_receive_decrypted_message";
    //Phone's capability
    //private static final String RECEIVE_DECRYPTED_CAPABILITY_NAME= "MessageTest_send_encrypted_message"

    private GoogleApiClient mGoogleApiClient;



    @Override
    public void onCreate(){
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void onConnectedNodes(List<Node> connectedNodes) {
        // After we are notified by this callback, we need to query for the nodes that provide the
        // "find_me" capability and are directly connected.
        if (mGoogleApiClient.isConnected()) {
            setOrUpdateNotification();
        } else if (!mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
    }

    private void setOrUpdateNotification(){
        Wearable.CapabilityApi.getCapability(mGoogleApiClient, RECEIVE_DECRYPTED_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE).setResultCallback(new ResultCallback<CapabilityApi.GetCapabilityResult>() {
            @Override
            public void onResult(CapabilityApi.GetCapabilityResult result) {
                if (result.getStatus().isSuccess()){
                    updateMessageCapability(result.getCapability());
                }else{
                    Log.e(TAG,
                            "setOrUpdateNotification() Failed to get capabilities, "
                                    + "status: "
                                    + result.getStatus().getStatusMessage());
                }
            }
        });
    }

    private void updateMessageCapability(CapabilityInfo capabilityInfo){
        Set<Node> connectedNodes = capabilityInfo.getNodes();
        if (connectedNodes.isEmpty()) {
            setupLostConnectivityNotification();
        } else {
            for (Node node : connectedNodes) {
                // we are only considering those nodes that are directly connected
                if (node.isNearby()) {
                    ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                            .cancel(MESSAGE_RECIEVED_NOTIFICATION_ID);
                }
            }
        }
    }

    private void setupLostConnectivityNotification() {
        Notification.Builder notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Title")
                .setContentText("Text")
                .setVibrate(new long[]{0, 200})  // Vibrate for 200 milliseconds.
                //.setSmallIcon(R.mipmap.ic_launcher)
                .setLocalOnly(true)
                .setPriority(Notification.PRIORITY_MAX);
        Notification card = notificationBuilder.build();
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                .notify(MESSAGE_RECIEVED_NOTIFICATION_ID, card);
    }





    //Connection Callback methods
    @Override
    public void onConnected(Bundle bundle) {
        setOrUpdateNotification();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent){
        System.out.println("Touchdown. Message received.");//You left off here. This should be the bit that sends it.
        System.out.println("Message text: " + messageEvent.getData().toString());

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("message", messageEvent.getData().toString());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    @Override
    public void onDestroy(){
        if (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }
}
