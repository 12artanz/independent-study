package com.freelance.aa022396.messagetest;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by aa022396 on 3/14/16.
 */
public class MainActivity extends Activity{

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        mTextView = (TextView) findViewById(R.id.text);
    }
}
