package com.freelance.aa022396.simplemessage;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.Collection;

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
//        System.out.println(mGoogleApiClient.hasConnectedApi(Wearable.API));
//        System.out.println(isGPSAvailable());
        setContentView(R.layout.activity_main);


        }


    @Override
    protected void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
//        System.out.println("Shits connected");
//        System.out.println(mGoogleApiClient.isConnected());
    }

    @Override
    protected void onStop(){
        mGoogleApiClient.disconnect();
        System.out.println("Shits disconnected");
        super.onStop();

    }

    //Button Method
    public void onSendMessage(View view){
        System.out.println("Button works.");
    }



    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("Shits connected for real");
        System.out.println("Here's the proof");
        System.out.println(mGoogleApiClient.isConnected());
        System.out.println(mGoogleApiClient.hasConnectedApi(Wearable.API));


    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("Shits suspended");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("shits stuck");
    }


    private boolean isGPSAvailable(){
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(ConnectionResult.SUCCESS == status){
            return true;
        }else{
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }

    }
}
