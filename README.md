# README #
This is a project based on an Independent Study for a class. The project intends to make an android application that uses Android Wear to make data on mobile devices more secure. The app works by encrypting files on the phone and storing the keys on the watch. It also uses GPS coordinates to keep from handing out keys unless you are at a specific place.

The actual project is under the Ping_Pong_Final directory. The other folders are testing implementations of various functionalities so that I could familiarize myself with them. 


### Who do I talk to? ###

Arturo Anzures, Jr.
arturoanzures102@yahoo.com

I'd be glad to answer any questions.