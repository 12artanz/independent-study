package com.freelance.aa022396.ping_pongv2;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void sendPing(View view){
        Toast.makeText(this, "Ping sent.", Toast.LENGTH_SHORT).show();

        //replayIntent goes here
        Intent actionIntent = new Intent(this, MyDisplayActivity.class);
        PendingIntent actionPendingIntent =
                PendingIntent.getActivity(this, 0, actionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //Create action
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.ic_media_play,
                        "Send Pong", actionPendingIntent)
                .build();

        //Create notification
        Notification notification = new NotificationCompat.Builder(getApplication())
                .setSmallIcon(R.drawable.powered_by_google_dark)
                .setContentTitle("Hello World")
                .setContentText("My first Android Wear notification")
                .extend(new NotificationCompat.WearableExtender().addAction(action)/*NotificationCompat.WearableExtender().setHintShowBackgroundOnly(true)*/)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplication());

        final int notification_Id=001;
        notificationManager.notify(notification_Id, notification);
    }
}
